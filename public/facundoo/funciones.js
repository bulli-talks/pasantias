function olla(){
    document.getElementById("texto1").innerHTML = "Dentro de los metodos de coccion en medio acuoso o de coccion humeda, uno de los mas utilizados y antiguos es el hervido. Hervir consiste en cocer un alimento mediante la inmersion en liquido (generalmente en agua o caldo) en ebullicion durante un cierto tiempo, este dependera de los alimentos a cocinar.";
}
function parrilla(){
    document.getElementById("texto1").innerHTML = "Cocinar a la parrilla significa elaborar recetas donde los ingredientes se cocinaran de una forma muy natural y sin necesidad de usar aditivos. De hecho, para cocinar platos a la parrilla no es necesario usar condimentos como el aceite. La parrilla implica el uso de carbon y, por tanto, consigue sabores mas ahumados.";
}
function sal(){
    document.getElementById("texto1").innerHTML = "Cocinar a la sal es uno de los metodos de coccion antiguos que ademas resulta muy sano. Este tipo de coccion conserva los nutrientes del ingrediente que se cocina y a diferencia de lo que pueda parecer, cocinar a la sal no proporciona un plato muy salado, el pescado, la carne o lo que se desee cocinar a la sal, absorbera la cantidad justa y necesaria de este condimento.";
}
function horno(){
    document.getElementById("texto1").innerHTML = "Esta forma de cocinar es una de las mas faciles debido a que lo unico que se nescesita es paciencia, uno puede regular la temperatura de la coccion. Muy recomendado para usar con carnes y verduras. Ademas, se puede poner papel aluminio para incrementar aun mas la caracteristica de envolver el aire y con esto sacar comida mucho mas tierna.";
}
function chifonada(){
    document.getElementById("texto1").innerHTML = " Origen: La palabra proviene del frances chiffonner que significa arrugar. Descripción: Corte fino para las hojas. En algunos casos se quita la vena, se sobrepone una sobre otra, se en rolla y procede con el corte. Son de 2 a 3 milímetros de grosor, lo que produce 'hilos' de verdura denominados chifonade.";
}
function baston(){
    document.getElementById("texto1").innerHTML = "Origen: Frances. Se utiliza principalmente en papas fritas y en otras verduras para guarnición (zanahorias). Primero se cortan rebanadas y luego los bastones tratando que el tamano sea uniforme. Es de un Corte rectangular de 6 a 7 cm. de largo por 1 cm. de ancho. "
}
function chips(){
    document.getElementById("texto1").innerHTML = "Tajadas redondas muy finas. Generalmente se usa en papas, camotes, platanos. El corte es mas parejo y preciso si se utiliza mandolina."
}
function ham(){
    document.getElementById("texto1").innerHTML = "El jamon serrano es un alimento obtenido a partir de la salazon y secado al aire de las patas traseras del cerdo. Este mismo producto recibe tambien el nombre de paleta o paletilla cuando se obtiene de las patas delanteras. "
}
function bacon(){
    document.getElementById("texto1").innerHTML = "El tocino es el acumulo graso que se deposita en la porcion subcutanea de la piel del cerdo. Tambien se denomina lardo y se describe como parte grasa que esta entre el cuero y la carne de cerdo y que cubre todo el cuerpo de animal."
}
function ribs(){
    document.getElementById("texto1").innerHTML = "Se caracteriza por ofrecer una alternancia de hueso y carne que habitualmente se cocinan juntos. Una variante es el spareribs (costillas esternales) que contiene una porcion de carne menor. Siendo una de las versiones más populares en muchas culturas el asado a la barbacoa."
}
function carre(){
    document.getElementById("texto1").innerHTML = "El termino carre es utilizado principalmente para el cordero a pesar de que se pueda aplicar a otros animales de ganado, siendo ademas una forma mas fina de definir el costillar, al menos en nuestra gastronomia."
}
function tbone(){
    document.getElementById("textoesp").innerHTML = " Se trata de un peculiar corte que no se da en nuestro entorno, sino en paises de lengua anglosajona.Se realiza de forma transversal a las costillas y tiene como resultado un chuleton con un hueso en forma de T en el medio; que separa dos partes diferentes de carne, una a cada lado del hueso y ambas exquisitas. Deciamos al principio que el T-bone es un corte maravilloso porque reune dos piezas de carne suculentas: el solomillo y el entrecot. La parte mas chica es la del primero y la de mayor volumen del segundo."
}
function ribeye(){
    document.getElementById("textoesp").innerHTML = "El ojo de bife o Ribeye como se conoce en Estados Unidos, es uno de los cortes vacunos mas populares y jugosos en el mercado. La carne de la seccion de la costilla es más blanda y mas grasa que la mayoria de los cortes de carne de vacuno."
}










function resultado() {
    var aciertos = 0;
    var selected1 = obtenerValor("rta1")
    var selected2 = obtenerValor("rta2")
    var selected3 = obtenerValor("rta3")
    var selected4 = obtenerValor("rta4")
    var selected5 = obtenerValor("rta5")
    var selected6 = obtenerValor("rta6")
    var selected7 = obtenerValor("rta7")
    var selected8 = obtenerValor("rta8")
    var selected9 = obtenerValor("rta9")
    var selected10 = obtenerValor("rta10")
    var selected11 = obtenerValor("rta11")
    var selected12 = obtenerValor("rta12")
    var selected13 = obtenerValor("rta13")



    if( selected1 == "baston"){
        aciertos += 1;
        document.getElementById("correcta1").style.backgroundColor = "green" ;
    }else {
        if (selected1 == "vichy"){
        document.getElementById("mal1").style.background = "red";
        document.getElementById("correcta1").style.backgroundColor = "green" ;
        } else {
            if(selected1 == "cascos"){
            document.getElementById("mal2").style.background = "red";
            document.getElementById("correcta1").style.backgroundColor = "green" ;

            }
        }
    }
    if( selected2 == "horno"){
        aciertos += 1;
        document.getElementById("correcta2").style.backgroundColor = "green" ;
    }   else {
        }   
        if (selected2 == "fritura"){
        document.getElementById("mal3").style.background = "red";
        document.getElementById("correcta2").style.backgroundColor = "green" ;

        } else {
            if (selected2 == "hervido"){
            document.getElementById("mal4").style.background = "red";
            document.getElementById("correcta2").style.backgroundColor = "green" ;
            }
          }

    if ( selected3 =="costilla"){
        aciertos += 1 ;
        document.getElementById("correcta3").style.backgroundColor = "green" ;
    } else {
        }   
        if (selected3 == "matambre"){
        document.getElementById("mal5").style.background = "red";
        document.getElementById("correcta3").style.backgroundColor = "green" ;

        } else {
            if (selected3 == "solomillo"){
            document.getElementById("mal6").style.background = "red";
            document.getElementById("correcta3").style.backgroundColor = "green" ;
            }
          }

    if ( selected4 =="ojob"){
        aciertos += 1 ;
        document.getElementById("correcta4").style.backgroundColor = "green" ;
    } else {
        }   
        if (selected4 == "vacio"){
        document.getElementById("mal7").style.background = "red";
        document.getElementById("correcta4").style.backgroundColor = "green" ;

        } else {
            if (selected4 == "bondiola"){
            document.getElementById("mal8").style.background = "red";
            document.getElementById("correcta4").style.backgroundColor = "green" ;
            }
          }
    if ( selected5 =="tocino"){
        aciertos += 1 ;
        document.getElementById("correcta5").style.backgroundColor = "green" ;
    } else {
        }   
        if (selected5 == "jamon"){
        document.getElementById("mal9").style.background = "red";
        document.getElementById("correcta5").style.backgroundColor = "green" ;

        } else {
            if (selected5 == "matambre"){
            document.getElementById("mal10").style.background = "red";
            document.getElementById("correcta5").style.backgroundColor = "green" ;
            }
          }
    if ( selected6 =="salteado"){
        aciertos += 1 ;
        document.getElementById("correcta6").style.backgroundColor = "green" ;

    } else {
        }   
        if (selected6 == "parrilla"){
        document.getElementById("mal11").style.background = "red";
        document.getElementById("correcta6").style.backgroundColor = "green" ;

        } else {
            if (selected6 == "plancha"){
            document.getElementById("mal12").style.background = "red";
            document.getElementById("correcta6").style.backgroundColor = "green" ;
            }
          }
    if ( selected7 =="hervido"){
        aciertos += 1; 
        document.getElementById("correcta7").style.backgroundColor = "green" ;
    } else {
        }   
        if (selected7 == "horno"){
        document.getElementById("mal13").style.background = "red";
        document.getElementById("correcta7").style.backgroundColor = "green" ;

        } else {
            if (selected7 == "salteado"){
            document.getElementById("mal14").style.background = "red";
            document.getElementById("correcta7").style.backgroundColor = "green" ;
            }
          }
    if ( selected8 =="pescado"){
        aciertos += 1 ;
        document.getElementById("correcta8").style.backgroundColor = "green" ;

    } else {
        }   
        if (selected8 == "pollo"){
        document.getElementById("mal15").style.background = "red";
        document.getElementById("correcta8").style.backgroundColor = "green" ;

        } else {
            if (selected8 == "carne"){
            document.getElementById("mal16").style.background = "red";
            document.getElementById("correcta8").style.backgroundColor = "green" ;
            }
          }
    if ( selected9 =="chifonada"){
        aciertos += 1 ;
        document.getElementById("correcta9").style.backgroundColor = "green" ;
    } else {
        }   
        if (selected9 == "juliana"){
        document.getElementById("mal17").style.background = "red";
        document.getElementById("correcta9").style.backgroundColor = "green" ;

        } else {
            if (selected9 == "brunoise"){
            document.getElementById("mal18").style.background = "red";
            document.getElementById("correcta9").style.backgroundColor = "green" ;
            }
          }
    if ( selected10 =="carre"){
        aciertos += 1 ;
        document.getElementById("correcta10").style.backgroundColor = "green" ;
    } else {
        }   
        if (selected10 == "solomillo"){
        document.getElementById("mal19").style.background = "red";
        document.getElementById("correcta10").style.backgroundColor = "green" ;

        } else {
            if (selected10 == "costilla"){
            document.getElementById("mal20").style.background = "red";
            document.getElementById("correcta10").style.backgroundColor = "green" ;
            }
          }
    if ( selected11 =="solomillo"){
        aciertos += 1 ;
        document.getElementById("correcta11").style.backgroundColor = "green" ;
    }  else {
        }   
        if (selected11 == "vacio"){
        document.getElementById("mal21").style.background = "red";
        document.getElementById("correcta11").style.backgroundColor = "green" ;

        } else {
            if (selected11 == "matambre"){
            document.getElementById("mal22").style.background = "red";
            document.getElementById("correcta11").style.backgroundColor = "green" ;
            }
          }
    if ( selected12 =="bifec"){
        aciertos += 1 ;
        document.getElementById("correcta12").style.backgroundColor = "green" ;
    } else {
        }   
        if (selected12 == "ojob"){
        document.getElementById("mal23").style.background = "red";
        document.getElementById("correcta12").style.backgroundColor = "green" ;

        } else {
            if (selected12 == "lomo"){
            document.getElementById("mal24").style.background = "red";
            document.getElementById("correcta12").style.backgroundColor = "green" ;
            }
          }
    if ( selected13 =="brunoise"){
        aciertos += 1 ;
        document.getElementById("correcta13").style.backgroundColor = "green" ;
    } else {
        }   
        if (selected13 == "chip"){
        document.getElementById("mal25").style.background = "red";
        document.getElementById("correcta13").style.backgroundColor = "green" ;

        } else {
            if (selected13 == "vichy"){
            document.getElementById("mal26").style.background = "red";
            document.getElementById("correcta13").style.backgroundColor = "green" ;
            }
          }
    
      document.getElementById("total").innerHTML = "Has acertado  "+ (aciertos) + "  veces";
    }

function obtenerValor(nombreOpciones) {
    var respuesta1 = document.getElementsByName(nombreOpciones);
    
    var selected1 = null;
    for(var i=0;i<3;i++) {
        if(respuesta1[i].checked == true) {
            selected1 = respuesta1[i].value;
        }  
    }
    return selected1;
}
